/**
 * If the number is divisible by 5 and 7, return 'fizzbuzz'. If the number is divisible by 5, return
 * 'buzz'. If the number is divisible by 7, return 'fizz'. Otherwise, return an empty string
 * 
 * @param num The number to be checked.
 * 
 * @return the string "fizzbuzz" if the number is divisible by 5 and 7, "buzz" if the number is
 * divisible by 5, and "fizz" if the number is divisible by 7.
 */

const fizzbuzz = (num) => {

/* This is a conditional statement that checks if the number is divisible by 5 and 7, if it is, it
returns "fizzbuzz". If the number is divisible by 5, it returns "buzz". If the number is divisible
by 7, it returns "fizz". If the number is not divisible by 5 or 7, it returns an empty string. */
    if (num % 5 === 0 && num % 7 === 0) {
        return 'fizzbuzz';
    } else if (num % 5 === 0) {
        return 'buzz'
        ;}else if (!num ) {
        return "Error!";
    }

    return (num % 5 == 0 ? "buzz" : (num % 7 == 0 ? "fizz" : ""));
}

exports.fizzbuzz = fizzbuzz;