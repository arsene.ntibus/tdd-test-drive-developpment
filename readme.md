- 1. [Qu'elle est le principe du TDD ?](#QuelleestleprincipeduTDD)
  - 1.1. [ Quel est le bénfice du TDD ?](#QuelestlebnficeduTDD)

# TDD

- Cloner ce repo
- Rechercher et expliquer avec vos mots les principes du tdd, et l'ajouter a ce Readme.
- Observer fizzbuzz.test.js puis completer fizzbuzz.js pour qu'il passe les tests ("npm run test" pour lancer les tests)
- Creer un fichier calc.js dans /src contenant une fonction calc()
- Creer un fichier calc.test.js dans /test
- Ecrire les test d'une fonction calc qui permet d'additionner/soustraire/multiplier/diviser 2 nombres envoyé a la fonction calc,
  avec gestion de toutes les erreurs possibles, en utilisant la méthode tdd.
- commit entre chaque ecriture de test et chaque ecriture de fonction

## 1. <a name='QuelleestleprincipeduTDD'></a>Qu'elle est le principe du TDD ?

Le TDD est un processus de développement qui consiste à développer une application en utilisant des tests unitaires.

il y a trois phases dans le TDD:
     1. La phase de test :
        Le développeur teste le code en utilisant des tests unitaires.
            Les tests unitaires sont des programmes qui vérifient que le code fonctionne correctement.

    2. La correction du code :
        Lorsque un des tests unitaires échoue, le développeur corrige le code juqu'à ce que tous les tests unitaires passent.


    3. La phase de refatorisation:
        Une fois que tous les tests unitaires passent, le développeur refactorise le code pour qu'il soit plus performant, et qu'il soit plus facile à maintenir.

## 1.1. <a name='QuelestlebnficeduTDD'></a> Quel est le bénfice du TDD ?

* Le TDD sert à n'ecrire que du code qui est nécessaire pour le développement, il a une approche plus agile et plus rapide.


 * Il est modulaire, c'est à dire  qu'on ne considère  qu'une microfonctionnalité à chaque impmlémentation, et non une fonctionnalité complète.
 
## 1.2 Quel Inconvénient ?
* Il est difficile de tester une fonctionnalité qui n'est pas encore implémentée. 
* Le procécus peut etre long et compliqué.
* 